$(function(){
			$("[data-toggle='tooltip']").tooltip();
			$("[data-toggle='popover']").popover();
			$(".carousel").carousel({
				interval: 3000
			});

			$('#contacto').on('show.bs.modal', function (e) {
				console.log('el modal se esta mostrando...');
				$('#contactoBtn').removeClass('btn-outline-success');
				$('#contactoBtn').addClass('btn-primary');
				$('#contactoBtn').prop('disabled', true);

			});
			$('#contacto').on('shown.bs.modal', function (e) {
				console.log('el modal se esta mostro');
			});
			$('#contacto').on('hide.bs.modal', function (e) {
				console.log('el modal se esta ocultando...');
				$('#contactoBtn').removeClass('btn-primary');
				$('#contactoBtn').addClass('btn-outline-success');
				$('#contactoBtn').prop('disabled', false);
			});
			$('#contacto').on('shown.bs.modal', function (e) {
				console.log('el modal se esta oculto');

			});

		});